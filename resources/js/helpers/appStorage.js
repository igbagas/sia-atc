class appStorage {

    //Set Item Token dan User
    storeToken(token){
        localStorage.setItem('token', token);
    }

    storeUser(user){
        localStorage.setItem('user', user);
    }

    //Simpan Item Token dan user
    store(token, user){
        this.storeToken(token)
        this.storeUser(user)
    }

    //Bersihkan Item yang udah diSet
    clear(){
        localStorage.removeItem('token', token);
        localStorage.removeItem('user', user);
    }

    //Dapatkan token yang udah distore.
    getToken(){
        localStorage.getItem(token);
    }

    //Dapatkan user yang udah distore.
    getUser(){
        localStorage.getItem(user);
    }

}

export default appStorage = new appStorage();